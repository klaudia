#!/usr/bin/make -f
# Makefile for Klaudia #
# ---------------------- #
# Created by falkTX
#

DESTDIR =


all: build

build:
	# Generate python code from QtDesigner UI files
	pyuic4 -o ./src/ui_klaudia.py ./src/klaudia.ui

clean:
	rm -f src/ui_*.py src/*.pyc src/*~ *~

install:
	# Make directories
	install -d $(DESTDIR)/usr/bin/
	install -d $(DESTDIR)/usr/share/applications/
	install -d $(DESTDIR)/usr/share/icons/hicolor/16x16/apps/
	install -d $(DESTDIR)/usr/share/icons/hicolor/32x32/apps/
	install -d $(DESTDIR)/usr/share/icons/hicolor/48x48/apps/
	install -d $(DESTDIR)/usr/share/icons/hicolor/128x128/apps/
	install -d $(DESTDIR)/usr/share/klaudia/src/
	install -d $(DESTDIR)/usr/share/klaudia/icons/
	install -d $(DESTDIR)/usr/share/klaudia/templates/
	# Install files
	install -m 755 klaudia $(DESTDIR)/usr/bin/
	install -m 644 klaudia.desktop $(DESTDIR)/usr/share/applications/
	install -m 644 art/16.png $(DESTDIR)/usr/share/icons/hicolor/16x16/apps/klaudia.png
	install -m 644 art/32.png $(DESTDIR)/usr/share/icons/hicolor/32x32/apps/klaudia.png
	install -m 644 art/48.png $(DESTDIR)/usr/share/icons/hicolor/48x48/apps/klaudia.png
	install -m 644 art/128.png $(DESTDIR)/usr/share/icons/hicolor/128x128/apps/klaudia.png
	install -m 755 src/*.py $(DESTDIR)/usr/share/klaudia/src/
	cp -r icons/* $(DESTDIR)/usr/share/klaudia/icons/
	cp -r templates/* $(DESTDIR)/usr/share/klaudia/templates/
