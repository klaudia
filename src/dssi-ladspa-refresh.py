#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Small script to get all available LADSPA and DSSI, and
# output them in a klaudia-like database format

import os
from commands import getoutput

have_ladspa = False
have_dssi = False

ladspa_plugins = []
dssi_plugins = []

path = os.getenv("PATH").split(":")
for i in range(len(path)):
  if (not have_ladspa and os.path.exists(os.path.join(path[i], "listplugins"))):
    have_ladspa = True
  if (not have_dssi and os.path.exists(os.path.join(path[i], "dssi_list_plugins"))):
    have_dssi = True

if (have_ladspa):
  print "processing ladspa plugins..."
  ladspa_plugin_dump = getoutput("env WINEDEBUG=-all listplugins")

  LIB = None
  PACKAGE = None

  list_by_n = ladspa_plugin_dump.split("\n")

  for i in range(len(list_by_n)):
    if (list_by_n[i][0] == '/'):
      LIB = list_by_n[i].rsplit(":", 1)[0]
      PACKAGE = getoutput("dpkg -S %s" % (list_by_n[i].rsplit(":", 1)[0])).split(": ")[0]
      DESCRIPTION = getoutput("dpkg -s %s" % PACKAGE)

      if (LIB == "dssi-vst.so" or PACKAGE == "dssi-vst" or PACKAGE == "dpkg"):
        LIB = None
        PACKAGE = None

      if ("Homepage: " in DESCRIPTION):
        WEBSITE = DESCRIPTION.split("Homepage: ")[1].split("\n")[0]
      else:
        WEBSITE = ""

    elif (list_by_n[i][0:1] == '\t'):
      if (LIB == None or PACKAGE == None):
        continue

      LABEL = list_by_n[i].split("\t")[1].split("/")[-1].replace(")","")
      NAME = list_by_n[i].split("\t")[1].split("/"+LABEL+")", 1)[0].rsplit("(",1)[0].strip()

      print "processing", NAME+"..."

      # Package || Plugin Name || Type || Binary.so:Label || Icon || Mode || Level || License || Features[ built-in-fx, stereo, midi-mode ] || Doc[ doc, website ]
      ladspa_plugins.append('  ( "%s", "%s", "Effect", "%s:%s", generic_audio_icon, "LADSPA", 0, "OpenSource", (1, 0, "Jack"), ("", "%s") ),' % (
        PACKAGE, NAME, LIB, LABEL, WEBSITE))

if (have_dssi):
  print "processing dssi plugins..."
  dssi_plugin_dump = getoutput("env WINEDEBUG=-all dssi_list_plugins")

  LIB = None
  PACKAGE = None

  list_by_n = dssi_plugin_dump.split("\n")

  for i in range(len(list_by_n)):
    if (list_by_n[i][0] == '/'):
      LIB = list_by_n[i].split("/")[-1]
      PACKAGE = getoutput("dpkg -S %s" % (list_by_n[i])).split(": ")[0]
      DESCRIPTION = getoutput("dpkg -s %s" % PACKAGE)

      if (LIB == "dssi-vst.so" or PACKAGE == "dssi-vst" or PACKAGE == "dpkg"):
        LIB = None
        PACKAGE = None

      if ("Homepage: " in DESCRIPTION):
        WEBSITE = DESCRIPTION.split("Homepage: ")[1].split("\n")[0]
      else:
        WEBSITE = ""

    elif (list_by_n[i][0:1] == '\t'):
      if (LIB == None or PACKAGE == None):
        continue

      LABEL = list_by_n[i].split("\t")[1].split(" ")[0]
      NAME = list_by_n[i].split("\t")[1].split(LABEL, 1)[1].strip()

      print "processing", NAME+"..."

      # Package || Plugin Name || Type || Binary.so:Label || Icon || Mode || Level || License || Features[ built-in-fx, stereo, midi-mode ] || Doc[ doc, website ]
      dssi_plugins.append('  ( "%s", "%s", "Synth", "%s:%s", generic_audio_icon, "DSSI", 0, "OpenSource", (FX, ST, "Jack"), ("", "%s") ),' % (
        PACKAGE, NAME, LIB, LABEL, WEBSITE))

ladspa_plugins.sort()
dssi_plugins.sort()

print "done"
print
print
print
print
print "LADSPA"
print "-----------------------------------------------"
for i in range(len(ladspa_plugins)):
  print ladspa_plugins[i]
print "-----------------------------------------------"
print
print "DSSI"
print "-----------------------------------------------"
for i in range(len(dssi_plugins)):
  print dssi_plugins[i]
print "-----------------------------------------------"

