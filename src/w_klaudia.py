#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports (Global)
import dbus, os, sys
from PyQt4.QtCore import Qt, QTimer, QSettings, QString, QVariant, SIGNAL
from PyQt4.QtGui import QFileDialog, QIcon, QMainWindow, QMessageBox

# Imports (Custom)
import jacklib, ui_klaudia
from klaudia_launcher import KlaudiaLauncher

# Properly convert QString to str
def QStringStr(string):
    return str(unicode(string).encode('utf-8'))

# QLineEdit and QPushButtom combo
def getAndSetPath(parent, current_path, lineEdit):
    new_path = QFileDialog.getExistingDirectory(parent, parent.tr("Set Path"), current_path, QFileDialog.ShowDirsOnly)
    if not new_path.isEmpty():
      lineEdit.setText(new_path)
    return new_path

# DBus connections
class DBus(object):
    __slots__ = [
            'loopBus',
            'controlBus',
            'studioBus',
            'appBus',
    ]
DBus = DBus()

# Main Window
class KlaudiaMainW(QMainWindow, ui_klaudia.Ui_KlaudiaMainW):
    def __init__(self, parent=None):
        super(KlaudiaMainW, self).__init__(parent)
        self.setupUi(self)

        # Load Settings (GUI state)
        self.settings = QSettings()
        self.loadSettings()

        self.launcher = KlaudiaLauncher(self, self.settings)

        # Check for Jack
        self.jack_client = jacklib.client_open("klaudia", jacklib.NoStartServer, 0)
        if not self.jack_client:
          QTimer.singleShot(0, self.showJackError)
          return

        # Set-up GUI
        self.b_start.setIcon(self.getIcon("go-next"))
        self.b_add.setIcon(self.getIcon("list-add"))
        self.b_refresh.setIcon(self.getIcon("view-refresh"))
        self.b_open.setIcon(self.getIcon("document-open"))
        self.b_start.setEnabled(False)
        self.b_add.setEnabled(False)
        self.setWindowIcon(self.getIcon("klaudia"))

        self.test_url = True
        self.test_selected = False
        self.studio_root_folder = QString(os.getenv("HOME"))

        self.le_url.setText(self.studio_root_folder)
        self.co_sample_rate.addItem(str(jacklib.get_sample_rate(self.jack_client)))
        self.b_refresh.setText("")

        pos = jacklib.jack_position_t()
        pos.valid = 0
        jacklib.transport_query(self.jack_client, pos)
        if (pos.valid & jacklib.PositionBBT):
          self.sb_bpm.setValue(pos.beats_per_minute)

        self.refreshStudioList()

        if (DBus.controlBus):
          self.enableLADISH(True)
        else:
          for path in os.getenv("PATH").split(":"):
            if (os.path.exists(os.path.join(path, "ladishd"))):
              break
          else:
            self.enableLADISH(False)

        self.connect(self.b_start, SIGNAL("clicked()"), self.launcher.startApp)
        self.connect(self.b_add, SIGNAL("clicked()"), self.launcher.addAppToLADISH)
        self.connect(self.b_refresh, SIGNAL("clicked()"), self.refreshStudioList)

        self.connect(self.co_ladi_room, SIGNAL("currentIndexChanged(int)"), self.checkSelectedRoom)
        self.connect(self.groupLADISH, SIGNAL("toggled(bool)"), self.enableLADISH)

        self.connect(self.le_url, SIGNAL("textChanged(QString)"), self.checkFolderUrl)

        self.connect(self.b_open, SIGNAL("clicked()"), lambda: getAndSetPath(self, self.le_url.text(), self.le_url))

    def showJackError(self):
        QMessageBox.critical(self, self.tr("Error"), self.tr("JACK is not started!\nPlease start it first, then re-run klaudia again."))
        self.close()

    def refreshStudioList(self):
        self.co_ladi_room.clear()
        self.co_ladi_room.addItem("<Studio Root>")
        if (DBus.controlBus):
          studio_bus = DBus.loopBus.get_object("org.ladish", "/org/ladish/Studio")
          studio_list_dump = studio_bus.GetRoomList()
          for i in range(len(studio_list_dump)):
            self.co_ladi_room.addItem(str(studio_list_dump[i][0]).replace("/org/ladish/Room","")+" - "+studio_list_dump[i][1]['name'])

    def checkSelectedRoom(self, co_n):
        if (co_n == -1 or not DBus.controlBus):
          pass
        elif (co_n == 0):
          DBus.studioBus = DBus.loopBus.get_object("org.ladish", "/org/ladish/Studio")
          DBus.appBus = dbus.Interface(DBus.studioBus, 'org.ladish.AppSupervisor')
          self.b_open.setEnabled(True)
          self.le_url.setEnabled(True)
          self.le_url.setText(self.studio_root_folder)
        else:
          room_number = QStringStr(self.co_ladi_room.currentText()).split(" ")[0]
          room_name = "/org/ladish/Room"+room_number
          DBus.studioBus = DBus.loopBus.get_object("org.ladish", room_name)
          DBus.appBus = dbus.Interface(DBus.studioBus, 'org.ladish.AppSupervisor')
          room_properties = DBus.studioBus.GetProjectProperties()
          if (len(room_properties[1]) > 0):
            self.b_open.setEnabled(False)
            self.le_url.setEnabled(False)
            self.le_url.setText(QString(room_properties[1]['dir']))
          else:
            self.b_open.setEnabled(True)
            self.le_url.setEnabled(True)
            self.studio_root_folder = self.le_url.text()

    def enableLADISH(self, yesno):
        self.groupLADISH.setCheckable(False)

        if (yesno):
          try:
            DBus.controlBus = DBus.loopBus.get_object("org.ladish", "/org/ladish/Control")
            self.groupLADISH.setTitle(self.tr("LADISH is enabled"))
          except:
            self.groupLADISH.setEnabled(False)
            self.groupLADISH.setTitle(self.tr("LADISH is sick"))
            return

          DBus.studioBus = DBus.loopBus.get_object("org.ladish", "/org/ladish/Studio")
          DBus.appBus = dbus.Interface(DBus.studioBus, 'org.ladish.AppSupervisor')

          self.refreshStudioList()
          self.checkGUI()

        else:
          self.groupLADISH.setEnabled(False)
          self.groupLADISH.setTitle(self.tr("LADISH is not available"))

    def checkGUI(self, test_selected=None):
        if (test_selected != None):
          self.test_selected = test_selected

        if (self.test_url and self.test_selected):
          self.b_add.setEnabled(bool(DBus.controlBus))
          self.b_start.setEnabled(True)
        else:
          self.b_add.setEnabled(False)
          self.b_start.setEnabled(False)

    def checkFolderUrl(self, qurl):
        url = QStringStr(qurl)
        if (os.path.exists(url)):
          self.test_url = True
          if (self.le_url.isEnabled()):
            self.studio_root_folder = url
        else:
          self.test_url = False
        self.checkGUI()

    def isRoom(self):
        return not self.le_url.isEnabled()

    def getAppBus(self):
        return DBus.appBus

    def getSampleRate(self):
        srate_try = self.co_sample_rate.currentText().toInt()
        return srate_try[0] if srate_try[1] else 44100

    def getBPM(self):
        bpm_try = self.sb_bpm.text().toInt()
        return bpm_try[0] if bpm_try[1] else 130.0

    def getProjectFolder(self):
        return self.le_url.text()

    def getIcon(self, icon):
        return QIcon.fromTheme(icon, QIcon("/usr/share/icons/oxygen/16x16/actions/%s.png" % (icon)))

    def saveSettings(self):
        self.settings.setValue("Geometry", QVariant(self.saveGeometry()))
        self.launcher.saveSettings()

    def loadSettings(self):
        self.restoreGeometry(self.settings.value("Geometry").toByteArray())

    def closeEvent(self, event):
        self.saveSettings()
        if (self.jack_client):
          jacklib.client_close(self.jack_client)
        return QMainWindow.closeEvent(self, event)
