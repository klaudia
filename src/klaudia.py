#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Test for python 3.x
if ("unicode" not in dir(__builtins__)):
  print("ERROR: This application is written for python2, please use that instead")
  quit()

# Imports (Global)
import dbus, os, signal, sys
from PyQt4.QtGui import QApplication

# Imports (Custom)
from w_klaudia import KlaudiaMainW, DBus

#--------------- main ------------------
if __name__ == '__main__':

    # App initialization
    app = QApplication(sys.argv)
    app.setApplicationName("Klaudia")
    app.setApplicationVersion("1.0")
    app.setOrganizationName("falkTX")

    # Do not close on SIGUSR1
    signal.signal(signal.SIGUSR1, signal.SIG_IGN)

    # Connect to DBus
    DBus.loopBus = dbus.SessionBus()

    if ("org.ladish" in DBus.loopBus.list_names()):
      DBus.controlBus = DBus.loopBus.get_object("org.ladish", "/org/ladish/Control")
      DBus.studioBus = DBus.loopBus.get_object("org.ladish", "/org/ladish/Studio")
      DBus.appBus = dbus.Interface(DBus.studioBus, "org.ladish.AppSupervisor")
    else:
      DBus.controlBus = None
      DBus.studioBus = None
      DBus.appBus = None

    # Show GUI
    gui = KlaudiaMainW()
    gui.show()

    # App-Loop
    sys.exit(app.exec_())
